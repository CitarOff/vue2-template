import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const locales = ['en-US', 'fr-FR'];

const i18n = new VueI18n({
  locale: locales[0],
  fallbackLocale: locales[0],
  silentTranslationWarn: process.env.NODE_ENV === 'production',
});

function loadLanguage(lang) {
  if (i18n.locale === lang) {
    return;
  }
  const currentLang = locales.includes(lang) ? lang : locales[1];
  i18n.setLocaleMessage(currentLang, locales[currentLang]);
  i18n.locale = currentLang;
  document.querySelector('html').setAttribute('lang', currentLang);
}

export { i18n, loadLanguage };
