module.exports = {
  css: {
    sourceMap: true,
    loaderOptions: {
      sass: {
        additionalData: `
          @import "@/assets/scss/responsive.scss";
        `
      }
    }
  },
}