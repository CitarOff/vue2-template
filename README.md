# Vue2 Template

This is a simple vue2 template with jest, vue-router, vuex, axios and vue-i18n

For the style, this template use SASS

## Content
- For testing : Jest (vue test utils)
- For lint : vue-cli-service
- For store : vuex
- For route : vue-router
- For lang : vue-i18n

## Installation
1. Clone this project
2. Delete .git folder
3. Run npm -i into your terminal
4. Go to dev ! 

## Usage
To run serve for dev locally
``` npm run serve```

To build your project
``` npm run build```

To run linter and fix CSS
``` npm run lint:css```

To run linter and fix JS
``` npm run lint:js```

To run test in dev mode (watch mode in jest)
``` npm run test:dev```

To run coverage test and check your coverage
``` npm run test:coverage```
