module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  testMatch: ['**/*.spec.(js|ts|tsx)'],
  testResultsProcessor: 'jest-sonar-reporter',
  collectCoverageFrom: ['src/**/*.(js|vue)'],
  coverageReporters: ['text', 'text-summary', 'lcov'],
  coveragePathIgnorePatterns: [
    '.i18n.js',
    '-types.js',
  ],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
};
