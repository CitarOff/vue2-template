import { shallowMount } from '@vue/test-utils';

import HelloWorld from './base-hello.vue';

const shallowComponent = (propsData = {}) => {
  return shallowMount(HelloWorld, {
    propsData: {
      ...propsData,
    },
  });
};

describe('HelloWorld component tests', () => {
  it('should mount the component', () => {
    const wrapper = shallowComponent();
    expect(wrapper).toBeDefined();
  });
});
